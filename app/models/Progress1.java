package models;
import java.util.*;
import javax.persistence.*;

import models.users.*;
import play.data.validation.*;
import play.data.format.*;
import com.avaje.ebean.*;

import static com.avaje.ebean.Expr.ilike;

@Entity
public class Progress1 extends Model{


    @Id
    private Long id;
    @Constraints.Required
    @Formats.DateTime(pattern="dd-MMM-yyyy")
    private Date date;
    @Constraints.Required
    @Constraints.Email
    private String email;
    @Constraints.Required
    private double height;
    @Constraints.Required
    private double weight;
    @Constraints.Required
    private double waist;
    @Constraints.Required
    private double thigh;
    @Constraints.Required
    private double bicep;
    @Constraints.Required
    private int age;
    @Constraints.Required
    private int planId;
    private double bmi;

    @ManyToOne
    private Member member;

    public Progress1() {
    }

    public static Finder<Long,Progress1> find = new Finder<Long,Progress1>(Progress1.class);

    public static List<Progress1> findAll(String mEmail) {
        return Progress1.find.where()
                .ilike("email", mEmail)
                .orderBy("date desc")
                .findList();

    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWaist() {
        return waist;
    }

    public void setWaist(double waist) {
        this.waist = waist;
    }

    public double getThigh() {
        return thigh;
    }

    public void setThigh(double thigh) {
        this.thigh = thigh;
    }

    public double getBicep() {
        return bicep;
    }

    public void setBicep(double bicep) {
        this.bicep = bicep;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public double getBmi() {
        return bmi;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

public double calculateBMI(){

bmi = weight/((height/100)*(height/100));
return bmi;
}
}
