package models;

import com.avaje.ebean.*;


import javax.persistence.*;
import java.util.*;


import models.users.*;



/**
 * Created by barba on 13/04/2017.
 */
@Entity
public class Timetable extends Model{
    @Id
    private Long id;

    private String day;
    private String fclass7am;
    private String fclass8am;
    private String fclass9am;
    private String fclass10am;
    private String fclass11am;
    private String fclass12pm;
    private String fclass1pm;
    private String fclass2pm;
    private String fclass3pm;
    private String fclass4pm;
    private String fclass5pm;
    private String fclass6pm;
    private String fclass7pm;
    private String fclass8pm;

    public Timetable(){}
    public static Finder<Long,Timetable> find = new Finder<Long,Timetable>(Timetable.class);


    public static List<Timetable> findAll() {
        return Timetable.find.all();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getFclass7am() {
        return fclass7am;
    }

    public void setFclass7am(String fclass7am) {
        this.fclass7am = fclass7am;
    }

    public String getFclass8am() {
        return fclass8am;
    }

    public void setFclass8am(String fclass8am) {
        this.fclass8am = fclass8am;
    }

    public String getFclass9am() {
        return fclass9am;
    }

    public void setFclass9am(String fclass9am) {
        this.fclass9am = fclass9am;
    }

    public String getFclass10am() {
        return fclass10am;
    }

    public void setFclass10am(String fclass10am) {
        this.fclass10am = fclass10am;
    }

    public String getFclass11am() {
        return fclass11am;
    }

    public void setFclass11am(String fclass11am) {
        this.fclass11am = fclass11am;
    }

    public String getFclass12pm() {
        return fclass12pm;
    }

    public void setFclass12pm(String fclass12pm) {
        this.fclass12pm = fclass12pm;
    }

    public String getFclass1pm() {
        return fclass1pm;
    }

    public void setFclass1pm(String fclass1pm) {
        this.fclass1pm = fclass1pm;
    }

    public String getFclass2pm() {
        return fclass2pm;
    }

    public void setFclass2pm(String fclass2pm) {
        this.fclass2pm = fclass2pm;
    }

    public String getFclass3pm() {
        return fclass3pm;
    }

    public void setFclass3pm(String fclass3pm) {
        this.fclass3pm = fclass3pm;
    }

    public String getFclass4pm() {
        return fclass4pm;
    }

    public void setFclass4pm(String fclass4pm) {
        this.fclass4pm = fclass4pm;
    }

    public String getFclass5pm() {
        return fclass5pm;
    }

    public void setFclass5pm(String fclass5pm) {
        this.fclass5pm = fclass5pm;
    }

    public String getFclass6pm() {
        return fclass6pm;
    }

    public void setFclass6pm(String fclass6pm) {
        this.fclass6pm = fclass6pm;
    }

    public String getFclass7pm() {
        return fclass7pm;
    }

    public void setFclass7pm(String fclass7pm) {
        this.fclass7pm = fclass7pm;
    }

    public String getFclass8pm() {
        return fclass8pm;
    }

    public void setFclass8pm(String fclass8pm) {
        this.fclass8pm = fclass8pm;
    }
}
