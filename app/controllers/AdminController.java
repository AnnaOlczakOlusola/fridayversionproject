package controllers;

import controllers.security.*;
import models.*;
import models.users.Member;
import models.users.User;
import play.db.ebean.Transactional;
import play.mvc.*;
import views.html.*;
import views.html.admin.*;
import views.html.admin.about;
import views.html.admin.contactUs;
import views.html.admin.index;
import views.html.admin.members;

import play.api.Environment;
import play.data.*;


import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import play.mvc.Http.*;
import play.mvc.Http.MultipartFormData.FilePart;
import java.io.File;



// Authenticate user
@Security.Authenticated(Secured.class)
// Authorise user (check if admin)
@With(AuthAdmin.class)
public class AdminController extends Controller {
private FormFactory formFactory;
private Environment env;
@Inject
    public AdminController(FormFactory f, Environment e){
        this.formFactory = f;
        this.env = e;
    }


    @Transactional
       private User getCurrentUser() {
        User u = User.getLoggedIn(session().get("email"));
        return u;
    }
@Transactional
    public Result vMembers() {
        List<Member> memberList = Member.findAll();
        return ok(members.render(getCurrentUser(), memberList));
    }

// Add a new Member
@Transactional
public Result addMem() {

Form<Member> addMemForm = formFactory.form(Member.class);

return ok(addMem.render(getCurrentUser(), addMemForm));
}

@Transactional
public Result addMemSubmit() {

Form<Member> newMemForm = formFactory.form(Member.class).bindFromRequest();

if(newMemForm.hasErrors()){

return badRequest(addMem.render(getCurrentUser(), newMemForm));
}

Member m = newMemForm.get();

if(m.getEmail() == null) {
m.save();

}

else if (m.getEmail() != null) {
m.update();
}



        

flash("Success", "You have registered/ updated a Member");

return redirect(controllers.routes.AdminController.vMembers());


}

//Update Member's details
    @Transactional
    public Result updateMem(String email) {

        Member m;
        Form<Member> memForm;

        try {
            // Find the member by id
            m = Member.find.byId(email);

            // Create a form based on the Product class and fill using p
            memForm = formFactory.form(Member.class).fill(m);

        } catch (Exception ex) {
            // Display an error message or page
            return badRequest("error");
        }
        // Render the updateMember view - pass form as parameter
        return ok(addMem.render(getCurrentUser(), memForm));
    }


    // Delete Member by email
    @Transactional
    public Result deleteMem(String email) {

        // find member by email and call delete method
        Member.find.ref(email).delete();
        // Add message to flash session
        flash("success", "Member has been deleted");

        // Redirect to products page
        return redirect(routes.AdminController.vMembers());
    }



    @Transactional
    public Result blog(String filter) {
        List<BlogPost> postsList = BlogPost.findAll(filter);
        return ok(blog.render(getCurrentUser(), postsList, env, filter));
    }

@Transactional
public Result addPost() {
        Form<BlogPost> addPostForm = formFactory.form(BlogPost.class);

        return ok(addPost.render(addPostForm, getCurrentUser()));
    }
@Transactional
    public Result addPostSubmit(){
        String saveImageMsg;
        Form<BlogPost> newPostForm = formFactory.form(BlogPost.class).bindFromRequest();

        if(newPostForm.hasErrors()){
            return badRequest(addPost.render(newPostForm, getCurrentUser()));
        }

        BlogPost newPost = newPostForm.get();

        if (newPost.getId() == null) {
            newPost.save();
        }
        else if (newPost.getId() != null) {
            newPost.update();
        }

        MultipartFormData data = request().body().asMultipartFormData();
        FilePart image = data.getFile("upload");

        saveImageMsg = saveFile(newPost.getId(), image);

        flash("success", "Blog Post has been created/ updated" + saveImageMsg);

        return redirect(routes.AdminController.index());
    }
@Transactional
    public String saveFile(Long id, FilePart image){
        if (image != null) {
            String mimeType = image.getContentType();
            if (mimeType.startsWith("image/")) {
                File file = (File) image.getFile();
                ConvertCmd cmd = new ConvertCmd();
                IMOperation op = new IMOperation();
                op.addImage(file.getAbsolutePath());
                op.resize(300, 200);
                op.addImage("public/images/postImages/" + id +".jpg");
                try{
                    cmd.run(op);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return " and image saved";
            }
        }
        return "image file missing";
    }
@Transactional
    public Result deletePost(Long id){

        BlogPost.find.ref(id).delete();

        flash("success", "Post has been deleted");

        return redirect(routes.AdminController.index());
    }
@Transactional
    public Result updatePost(Long id){
        BlogPost p;
        Form<BlogPost> postForm;

        try{
            p = BlogPost.find.byId(id);

            postForm = formFactory.form(BlogPost.class).fill(p);

        } catch (Exception ex) {
            return badRequest("error");
        }

        return ok(addPost.render(postForm, getCurrentUser()));
    }




    public Result video() {

        return ok(video.render(getCurrentUser()));
    }

    public Result diet() {

        return ok(diet.render(getCurrentUser()));
    }


    public Result index() { return ok (index.render(getCurrentUser()));}


    public Result about() { return ok (about.render(getCurrentUser()));}

    public Result contactUs() { return ok (contactUs.render(getCurrentUser()));}

    public Result progressList() {return ok (progressList.render(getCurrentUser()));}



@Transactional
    public Result timeSlots() {
 List<TimeSlot> timeSlotsList = TimeSlot.find.where()
                .ilike("places", "1")
.orderBy("date")
                .findList();


return ok(timeSlots.render(getCurrentUser(), timeSlotsList));
}
@Transactional
public Result addTimeSlot() {

Form<TimeSlot> addTimeSlotForm = formFactory.form(TimeSlot.class);

return ok(addTimeSlot.render(getCurrentUser(), addTimeSlotForm));
}
@Transactional
public Result addTimeSlotSubmit(){

Form<TimeSlot> newTimeSlotForm = formFactory.form(TimeSlot.class).bindFromRequest();

if(newTimeSlotForm.hasErrors()) {

return badRequest(addTimeSlot.render(getCurrentUser(), newTimeSlotForm));
}

TimeSlot p = newTimeSlotForm.get();
if(p.getId() == null) {
p.save();
}

else if (p.getId() != null) {
p.update();
}

flash("success", "Time Slot has been created/updated");

return redirect(controllers.routes.AdminController.timeSlots());

}
@Transactional
public Result deleteTimeSlot(Long id) {

TimeSlot.find.ref(id).delete();

flash("success", "Time Slot has been deleted");

return redirect (controllers.routes.AdminController.timeSlots());
}
@Transactional
public Result updateTimeSlot(Long id) {

TimeSlot p;
Form<TimeSlot>timeSlotForm;

try{
p = TimeSlot.find.byId(id);

timeSlotForm =formFactory.form(TimeSlot.class).fill(p);

} catch(Exception ex) {

return badRequest("Error");
}

return ok(addTimeSlot.render(getCurrentUser(), timeSlotForm));
}
    public Result timetable() {
        List<Timetable> timetableList = Timetable.findAll();

        return ok(views.html.admin.timetable.render(getCurrentUser(), timetableList));
    }

    public Result addTime() {

        Form<Timetable> addTimeForm = formFactory.form(Timetable.class);

        return ok(views.html.admin.addTime.render(getCurrentUser(), addTimeForm));
    }

    public Result addTimeSubmit(){

        Form<Timetable> newTimeForm = formFactory.form(Timetable.class).bindFromRequest();

        if(newTimeForm.hasErrors()) {

            return badRequest(views.html.admin.addTime.render(getCurrentUser(), newTimeForm));
        }

        Timetable p = newTimeForm.get();
        if(p.getId() == null) {
            p.save();
        }

        else if (p.getId() != null) {
            p.update();
        }

        flash("success", "Time has been created/updated");

        return redirect(controllers.routes.AdminController.timetable());

    }


    public Result updateTime(Long id) {

        Timetable p;
        Form<Timetable>timetableForm;

        try{
            p = Timetable.find.byId(id);

            timetableForm =formFactory.form(Timetable.class).fill(p);

        } catch(Exception ex) {

            return badRequest("Error");
        }

        return ok(views.html.admin.addTime.render(getCurrentUser(),timetableForm));
    }
    public Result vProgress(String email) {

        List<Progress1> progressList = Progress1.findAll(email);

        return ok(views.html.admin.progress.render(getCurrentUser(), progressList));
    }

    public Result addProgress() {

        Form<Progress1> addProgressForm = formFactory.form(Progress1.class);

        return ok(views.html.admin.addProgress.render(getCurrentUser(), addProgressForm));
    }

    public Result addProgressSubmit(){

        Form<Progress1> addProgressForm = formFactory.form(Progress1.class).bindFromRequest();

        if(addProgressForm.hasErrors()) {

            return badRequest(views.html.admin.addProgress.render(getCurrentUser(), addProgressForm));
        }

        Progress1 p = addProgressForm.get();
        if(p.getId() == null) {
p.calculateBMI();
            p.save();
        }

        else if (p.getId() != null) {
p.calculateBMI();
            p.update();
        }

        String email = p.getEmail();

        flash("success", "Progress has been created/updated");

        return redirect(controllers.routes.AdminController.vMembers());

    }

@Transactional
    public Result deleteProgress(Long id) {

        Progress1.find.ref(id).delete();

        flash("success", "Progress has been deleted");

        return redirect (controllers.routes.AdminController.vMembers());
    }
	
@Transactional
    public Result updateProgress(Long id) {

        Progress1 p;
        Form<Progress1>progressForm;

        try{
            p = Progress1.find.byId(id);

            progressForm =formFactory.form(Progress1.class).fill(p);

        } catch(Exception ex) {

            return badRequest("Error");
        }

        return ok(views.html.admin.addProgress.render(getCurrentUser(),progressForm));
    }


	public Result bookings(){

	List<BookingOrder> bookingsList = BookingOrder.findAll();
return ok(views.html.admin.bookings.render(getCurrentUser(), bookingsList));
	
}




public Result deleteBooking(Long id){
BookingOrder.find.byId(id).delete();

    flash("success", "Booking has been deleted");

    return redirect (controllers.routes.AdminController.bookings());
}
}


