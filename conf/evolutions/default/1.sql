# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table blog_post (
  id                            bigint not null,
  title                         varchar(255),
  category                      varchar(255),
  text                          TEXT,
  posted                        timestamp,
  constraint pk_blog_post primary key (id)
);
create sequence blog_post_seq;

create table booking_item (
  id                            bigint not null,
  order_id                      bigint,
  basket_id                     bigint,
  time_slot_id                  bigint,
  quantity                      integer,
  constraint pk_booking_item primary key (id)
);
create sequence booking_item_seq;

create table booking_order (
  id                            bigint not null,
  booking_date                  timestamp,
  member_email                  varchar(255),
  constraint pk_booking_order primary key (id)
);
create sequence booking_order_seq;

create table progress1 (
  id                            bigint not null,
  date                          timestamp,
  email                         varchar(255),
  height                        double,
  weight                        double,
  waist                         double,
  thigh                         double,
  bicep                         double,
  age                           integer,
  plan_id                       integer,
  bmi                           double,
  member_email                  varchar(255),
  constraint pk_progress1 primary key (id)
);
create sequence progress1_seq;

create table time_slot (
  id                            bigint not null,
  date                          timestamp,
  day                           varchar(255),
  time                          varchar(255),
  trainer                       varchar(255),
  duration                      varchar(255),
  places                        integer,
  constraint pk_time_slot primary key (id)
);
create sequence time_slot_seq;

create table timetable (
  id                            bigint not null,
  day                           varchar(255),
  fclass7am                     varchar(255),
  fclass8am                     varchar(255),
  fclass9am                     varchar(255),
  fclass10am                    varchar(255),
  fclass11am                    varchar(255),
  fclass12pm                    varchar(255),
  fclass1pm                     varchar(255),
  fclass2pm                     varchar(255),
  fclass3pm                     varchar(255),
  fclass4pm                     varchar(255),
  fclass5pm                     varchar(255),
  fclass6pm                     varchar(255),
  fclass7pm                     varchar(255),
  fclass8pm                     varchar(255),
  constraint pk_timetable primary key (id)
);
create sequence timetable_seq;

create table user (
  role                          varchar(255),
  email                         varchar(255) not null,
  first_name                    varchar(255),
  last_name                     varchar(255),
  password                      varchar(255),
  address_line1                 varchar(255),
  address_line2                 varchar(255),
  address_line3                 varchar(255),
  phone_number                  varchar(255),
  medical_condition             varchar(255),
  doctors_name                  varchar(255),
  doctors_phone                 varchar(255),
  emergency_name                varchar(255),
  emergency_phone               varchar(255),
  relationship                  varchar(255),
  security_question             varchar(255),
  security_answer               varchar(255),
  constraint pk_user primary key (email)
);

create table your_booking (
  id                            bigint not null,
  member_email                  varchar(255),
  constraint uq_your_booking_member_email unique (member_email),
  constraint pk_your_booking primary key (id)
);
create sequence your_booking_seq;

alter table booking_item add constraint fk_booking_item_order_id foreign key (order_id) references booking_order (id) on delete restrict on update restrict;
create index ix_booking_item_order_id on booking_item (order_id);

alter table booking_item add constraint fk_booking_item_basket_id foreign key (basket_id) references your_booking (id) on delete restrict on update restrict;
create index ix_booking_item_basket_id on booking_item (basket_id);

alter table booking_item add constraint fk_booking_item_time_slot_id foreign key (time_slot_id) references time_slot (id) on delete restrict on update restrict;
create index ix_booking_item_time_slot_id on booking_item (time_slot_id);

alter table booking_order add constraint fk_booking_order_member_email foreign key (member_email) references user (email) on delete restrict on update restrict;
create index ix_booking_order_member_email on booking_order (member_email);

alter table progress1 add constraint fk_progress1_member_email foreign key (member_email) references user (email) on delete restrict on update restrict;
create index ix_progress1_member_email on progress1 (member_email);

alter table your_booking add constraint fk_your_booking_member_email foreign key (member_email) references user (email) on delete restrict on update restrict;


# --- !Downs

alter table booking_item drop constraint if exists fk_booking_item_order_id;
drop index if exists ix_booking_item_order_id;

alter table booking_item drop constraint if exists fk_booking_item_basket_id;
drop index if exists ix_booking_item_basket_id;

alter table booking_item drop constraint if exists fk_booking_item_time_slot_id;
drop index if exists ix_booking_item_time_slot_id;

alter table booking_order drop constraint if exists fk_booking_order_member_email;
drop index if exists ix_booking_order_member_email;

alter table progress1 drop constraint if exists fk_progress1_member_email;
drop index if exists ix_progress1_member_email;

alter table your_booking drop constraint if exists fk_your_booking_member_email;

drop table if exists blog_post;
drop sequence if exists blog_post_seq;

drop table if exists booking_item;
drop sequence if exists booking_item_seq;

drop table if exists booking_order;
drop sequence if exists booking_order_seq;

drop table if exists progress1;
drop sequence if exists progress1_seq;

drop table if exists time_slot;
drop sequence if exists time_slot_seq;

drop table if exists timetable;
drop sequence if exists timetable_seq;

drop table if exists user;

drop table if exists your_booking;
drop sequence if exists your_booking_seq;

